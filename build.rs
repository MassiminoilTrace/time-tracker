use image;
use std::fs::File;
use std::io::Write;
use std::path::Path;
fn main()
{
    println!("cargo:rerun-if-changed=icon.png");

    let path = Path::new("icon.png");
    fn icon_to_bin(path: &std::path::Path) -> Vec<u8>
    {
        let image = image::open(path)
            .expect("Failed to open icon path")
            .into_rgba8();
            let rgba = image.into_raw();
        rgba
    }
    File::create(path.with_extension("bin"))
    .expect("Error creating output icon file")
    .write_all(
        &icon_to_bin(path)
    ).expect("Error writing to icon bin file");
}