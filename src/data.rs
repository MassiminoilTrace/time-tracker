/*
Copyright © 2023,2024 - Massimo Gismondi

This file is part of Time tracker.

Time tracker is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Time tracker is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Time tracker.  If not, see <http://www.gnu.org/licenses/>.
*/
use std::path::{Path, PathBuf};

pub const SECONDS_TIME_STEP: u64 = 10;


use airone::prelude::*;

use std::time::{SystemTime, UNIX_EPOCH};


#[derive(AironeDbDerive, PartialEq, Clone, Debug)]
pub struct Activity
{
    unique_id: usize,
    title: String,
    // How much time spent on this
    duration_seconds: u64,
    running: bool,
    /// When was the last start action on this element, in UNIX time?
    last_used_unix_sec: u64
}

pub struct ActivityDb(AironeDb<Activity>);
impl ActivityDb
{
    pub fn new() -> Self
    {
        #[cfg(target_os="linux")]
        let homedir = PathBuf::from(&std::env::var("HOME").unwrap()).join(Path::new(".time_tracker/"));
        #[cfg(target_os="windows")]
        let homedir = PathBuf::from(&std::env::var("APPDATA").unwrap()).join(Path::new("time_tracker\\"));

        std::fs::create_dir_all(&homedir).unwrap();

        let mut tmp = Self(AironeDb::new_with_custom_name(
            homedir.join("data").to_str().unwrap()
        ).unwrap());
        tmp.sort_by_last_use();
        return tmp;
    }

    pub fn get_all(&self) -> &[Activity]
    {
        return self.0.get_all();
    }

    /// I insert the new task at the top
    pub fn push(&mut self, name: &str)
    {
        let max_id = self
            .0
            .get_all()
            .iter()
            .map(|el| *el.get_unique_id())
            .max()
            .unwrap_or(0);

        self.0.insert(
            0,
            Activity {
                unique_id: max_id + 1,
                title: name.trim().to_string(),
                duration_seconds: 0,
                running: false,
                last_used_unix_sec: ActivityDb::seconds_from_epoch()
            }
        ).unwrap();
    }

    pub fn toggle_running(&mut self, index: usize)
    {
        for i in 0..self.0.len()
        {
            if i == index
            {
                let is_running = self.0[i].get_running().clone();
                self.0.get_mut(i).unwrap().set_running(!is_running).unwrap();
                self.0.get_mut(i).unwrap().set_last_used_unix_sec(ActivityDb::seconds_from_epoch()).unwrap();
            }
            else
            {
                self.0.get_mut(i).unwrap().set_running(false).unwrap()
            }
        }
    }

    pub fn stop_all_tasks(&mut self)
    {
        for i in 0..self.0.len()
        {
            self.0.get_mut(i).unwrap().set_running(false).unwrap();
        }
    }

    pub fn remove(&mut self, index: usize)
    {
        self.0.remove(index).unwrap();
    }

    /// Advances the clock of running tasks
    /// by
    pub fn update_clock(&mut self)
    {
        for i in 0..self.0.len()
        {
            let is_running = self.0[i].get_running().clone();
            if is_running
            {
                let sec = self.0[i].get_duration_seconds().clone();
                self.0.get_mut(i).unwrap()
                    .set_duration_seconds(sec + SECONDS_TIME_STEP).unwrap();
            }
        }
    }

    fn sort_by_last_use(&mut self)
    {
        // Bubble sort
        for _ in 0..self.0.len()
        {
            for j in 0..self.0.len() - 1
            {
                if self.0.get(j).unwrap().last_used_unix_sec
                    < self.0.get(j + 1).unwrap().last_used_unix_sec
                {
                    let tmp = self.0.remove(j + 1).unwrap();
                    self.0.insert(j, tmp).unwrap();
                }
            }
        }
    }

    fn seconds_from_epoch() -> u64
    {
        SystemTime::now()
            .duration_since(UNIX_EPOCH)
            .unwrap()
            .as_secs()
    }
}

pub fn seconds_to_str(sec: u64) -> String
{
    let minutes: u64 = sec / 60;
    // let hours = minutes/60;
    format!("{:0>2}h:{:0>2}m", minutes / 60, minutes % 60)
}

#[test]
fn seconds_formatting_test()
{
    assert_eq!(seconds_to_str(05), "00h:00m");
    assert_eq!(seconds_to_str(65), "00h:01m");
    assert_eq!(seconds_to_str(125), "00h:02m");

    assert_eq!(seconds_to_str(3659), "01h:00m");

    assert_eq!(seconds_to_str(3600 + 125), "01h:02m");

    assert_eq!(seconds_to_str(10 * 3600 + 125), "10h:02m");
}
