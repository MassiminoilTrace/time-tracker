/*
Copyright © 2023,2024 - Massimo Gismondi

This file is part of Time tracker.

Time tracker is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Time tracker is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Time tracker.  If not, see <http://www.gnu.org/licenses/>.
*/
#![allow(non_snake_case)]
#![windows_subsystem = "windows"]

use dioxus::{desktop::{tao::window::Icon, Config, WindowBuilder}, prelude::*};
mod data;

use data::AironeInterfaceGetterActivity;

fn main()
{
    LaunchBuilder::new()
        .with_cfg(
            Config::new()
                .with_custom_index(include_str!("index.html").to_string())
                .with_icon(
                    Icon::from_rgba(
                        include_bytes!("../icon.bin").to_vec(),
                        128,
                        128
                    ).expect("Error creating the icon, check the data and its dimensions")
                )
                .with_menu(None)
                .with_window(
                    WindowBuilder::new()
                    .with_title(
                        "Time tracker"
                    )
                )
        )
        .launch(App);
}

#[component]
fn App() -> Element
{
    let window = dioxus::desktop::use_window();
    rsx! {
        // style {dangerous_inner_html: include_str!("mini-default.min.css")}
        style {dangerous_inner_html: include_str!("assets/mvp.css")}
        style {dangerous_inner_html: include_str!("assets/my_css.css")}
        header
        {
            h1 {"Time tracker"}
        }
        main
        {
            DisplayList{}
        }
        footer
        {
            class: "sticky",
            p
            {
                "© Copyright 2023,2024 - Massimo Gismondi"
                br {}
                a {
                    onclick: move |_|
                    {
                        let dom = VirtualDom::new(LicensePopup);
                        window.new_window(dom, Default::default());
                    },
                    "License Information"
                }
            }
        }
    }
}

#[component]
fn DisplayList() -> Element
{
    let mut db = use_signal_sync(
        ||{
            let mut d =  data::ActivityDb::new();
            d.stop_all_tasks();
            d
        }
    );

    

    let _: Coroutine<()> = use_coroutine(|_| {
        async move {
            loop
            {
                tokio::time::sleep(std::time::Duration::from_secs(data::SECONDS_TIME_STEP)).await;
                db.write().update_clock();
            }
        }
    });

    let mut new_item_name = use_signal(|| String::new());

    let add_item_button = rsx!(
        div
        {
            class:"add-button",
                input
                {
                    r#type: "text",
                    value: "{new_item_name.read()}",
                    placeholder: "Name of new activity",
                    maxlength: 40,
                    oninput: move |ev|{
                        new_item_name.write().clear();
                        new_item_name.write().push_str(&ev.data().value());
                    }
                }
                button
                {
                    r#type: "button",
                    class: "primary",
                    onclick: move |_| {
                        if new_item_name.read().len() == 0
                        {
                            return;
                        }
                        db.write().push(
                            &new_item_name.read()
                        );
                        new_item_name.write().clear();
                    },
                    "+"
                }
        }
    );

    rsx!(
        {add_item_button}
        hr{}
        table
        {
            tr
            {
                th {}
                th {"Name"}
                th {"Time spent"}
                th {"State"}
            }
            for (k, el) in db.read().get_all().iter().enumerate()
            {
                DisplayElement{
                    key: "{el.get_unique_id()}",
                    el: el.clone(),
                    on_delete: move |_|{
                        db.write().remove(k)
                    },
                    on_toggle_running: move |_| {
                        db.write().toggle_running(k);
                    }
                }
            }
        }
    )
}


#[component]
fn DisplayElement(
    el: data::Activity,
    on_delete: EventHandler,
    on_toggle_running: EventHandler
) -> Element
{
    let toggle_run = rsx! {
        button
        {
            class: if *el.get_running() {"secondary"} else {""},
            onclick: move |_| on_toggle_running.call(()),
            if *el.get_running()
            {
                {rsx!("STOP")}
            }
            else
            {
                {rsx!("START")}
            }
        }
    };

    rsx!(
        tr
        {
            td
            {
                button
                {
                    class:"exit-button",
                    onclick: move |_| {
                        on_delete.call(());
                    },
                    "X"
                }
            }
            td{"{el.get_title()}"}
            td {
                "{data::seconds_to_str(*el.get_duration_seconds())}"
                if *el.get_running()
                {
                    {rsx!( div {class:"spinner"})}
                }
            }
            td {
                {toggle_run}
            }
        }
    )
}

#[component]
fn Markdown(content: String) -> Element
{
    use pulldown_cmark::Parser;
    let parser = Parser::new(&content);

    let mut html_buf = String::new();
    pulldown_cmark::html::push_html(&mut html_buf, parser);
    rsx!(div {
        dangerous_inner_html: "{html_buf}"
    })
}

#[component]
fn LicensePopup() -> Element
{
    rsx!(
        style {dangerous_inner_html: include_str!("assets/mvp.css")}
        main
        {
            h1
            {
                "Time tracker"
            }
            p
            {
                "Copyright © 2023,2024 - Massimo Gismondi, GNU AGPL3 or later version license."
            }
            p
            {
                "This project uses other software libraries as dependencies. Check the README file for the complete copyright information."
            }
            p
            {
                a {
                    href:"https://gitlab.com/MassiminoilTrace/time-tracker",
                    "Repository"
                }
            }
            h2
            {
                "License Information"
            }
            Markdown
            {
                content: "This program is free software: you can redistribute it and/or modify \
                it under the terms of the GNU Affero General Public License as published by \
                the Free Software Foundation, either version 3 of the License, or \
                (at your option) any later version. \
                \n\n \
                This program is distributed in the hope that it will be useful, \
                but WITHOUT ANY WARRANTY; without even the implied warranty of \
                MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the \
                GNU Affero General Public License for more details. \
                \n\n \
                You should have received a copy of the GNU Affero General Public License \
                along with this program.  If not, see <http://www.gnu.org/licenses/>."
            }
            hr{}
            details
            {
                summary {
                    "AGPL license text"
                }
                Markdown
                {
                    content: include_str!("assets/agpl-3.0.md")
                }
            }
        }
    )
}
