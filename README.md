# <img src="icon.png" width="32"> Time Tracker

Time Tracker is a task time tracking desktop program to measure the time you dedicate to an activity, using [airone](https://gitlab.com/MassiminoilTrace/airone) library as a persistence layer and [Dioxus](https://github.com/DioxusLabs/dioxus) to build the interface.

Currently **only works reliably on GNU/Linux** systems, as the save data in put in a hardcoded path in the user directory and it has not been tested enough on other systems.

If you want to try building it for Windows, I made it work using the `x86_64-pc-windows-gnu` target. Then, get somewhere the `WebView2Loader.dll` and put it next to your executable. You can get that library inside the `Microsoft.Web.WebView2` package from [nuget.org](https://nuget.org). This reduces compilation problems by avoiding the MSVC toolchain, especially for cross-compilation.

![](screenshot.png)

# Licensing and Copyright information

Copyright © 2023,2024 - Massimo Gismondi 

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

## Copyright of dependencies

This program uses other free software pieces as in this list. Let me know if I missed any attribution by mistake.

- [MVP stylesheet](https://andybrewer.github.io/mvp/), MIT (expat) license - Copyright © 2020 Andy Brewer
- [Dioxus framework](https://github.com/DioxusLabs/dioxus), MIT (expat) license - Copyright Dioxus contributors
- [Airone](https://gitlab.com/MassiminoilTrace/airone) database library, GNU AGPLv3 or later license - Copyright © 2022,2023 Massimo Gismondi
- the [image](https://github.com/image-rs/image) crate is only used during compilation, but it's not shipped nor used in the final executable.
- check the subdependencies of each project in each Cargo.toml file for the complete author list.
